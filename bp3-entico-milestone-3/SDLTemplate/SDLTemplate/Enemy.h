#pragma once
#include "GameObject.h"
#include "common.h"
#include "draw.h"
#include "SoundManager.h"
#include "Bullet.h"
#include <vector>
#include "util.h" 
#include "Player.h"

class Enemy :
    public GameObject
{
public: 
	Enemy();
	~Enemy();
	void start();
	void update();
	void draw();
	void setTarget(Player* player);
	void setPos(int x, int y);
private:
	SDL_Texture* texture;
	Mix_Chunk* sound;
	Player* target;

	int x;
	int y;
	// where the enemy is going to travel
	float dirX;
	float dirY;
	int width;
	int height;
	int speed;
	float dirChangeTime;
	float currDirTime;
	float reloadTime;
	float currReloadTime;

	std::vector<Bullet*> bullets;
};

