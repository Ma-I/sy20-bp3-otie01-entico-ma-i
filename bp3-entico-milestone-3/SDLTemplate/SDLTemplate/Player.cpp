#include "Player.h"
#include "Scene.h"

Player::~Player()
{
	// delete bullets on death/destruction
	for (int i = 0; i < bullets.size(); i++)
	{
		delete bullets[i];
	}
	bullets.clear();
}

void Player::start()
{
	texture = loadTexture("gfx/player.png");

	x = 100;
	y = 100;
	
	height = 0;
	width = 0;

	speed = 5;
	reloadTime = 8; // per frame (running at 60fps)
	currReloadTime = 0;

	SDL_QueryTexture(texture, NULL, NULL, &width, &height);

	sound = SoundManager::loadSound("sound/334227__jradcoolness__laser.ogg");
}

void Player::update()
{
	if (app.keyboard[SDL_SCANCODE_W])
	{
		y -= speed;
	}
	if (app.keyboard[SDL_SCANCODE_A])
	{
		x -= speed;
	}
	if (app.keyboard[SDL_SCANCODE_S])
	{
		y += speed;
	}
	if (app.keyboard[SDL_SCANCODE_D])
	{
		x += speed;
	}
	// decrement reloadTime
	if (currReloadTime > 0)
	{
		currReloadTime--;
	}

	// bullet
	if (app.keyboard[SDL_SCANCODE_F] && currReloadTime == 0)
	{
		SoundManager::playSound(sound);
		Bullet* bullet = new Bullet(x + width, y - 2 + height/2, 1, 0, 10);
		bullets.push_back(bullet);
		getScene()->addGameObject(bullet);

		currReloadTime = reloadTime;
	}

	// Memory management
	for (int i = 0; i < bullets.size(); i++)
	{
		if (bullets[i]->getposX() > SCREEN_WIDTH)
		{
			// cache variable to delete later
			Bullet* bulletToErase = bullets[i];
			bullets.erase(bullets.begin() + i);
			delete bulletToErase;

			// can't mutate vector while looping in it - will create crash if too many bullets
			// so, we must delete only one per frame
			// note: can avoid lag this way
			break;
		}
	}
}

void Player::draw()
{
	blit(texture, x, y);
}

int Player::getPosX()
{
	return x;
}

int Player::getPosY()
{
	return y;
}
