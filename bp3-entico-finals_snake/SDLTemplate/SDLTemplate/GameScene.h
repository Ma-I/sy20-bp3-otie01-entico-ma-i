#pragma once
#include "Scene.h"
#include "GameObject.h"
#include "SnakeHead.h"
#include "Food.h"
#include <vector>
#include "text.h"

class GameScene : public Scene
{
public:
	GameScene();
	~GameScene();
	void start();
	void draw();
	void update();
private:
	SnakeHead* player;
	 
	float spawnTime;
	float currSpawnTimer;
	std::vector<Food*> spawnedFood;

	void spawnFood();
	void checkForCollision();
};

