#include "SnakeHead.h"

void SnakeHead::start()
{
	texture = loadTexture("gfx/snakeHeadSmall.png");
	width = 0;
	height = 0;

	length = 0;
	speed = 5;
	xHeadPos = SCREEN_WIDTH / 2;
	yHeadPos = SCREEN_HEIGHT / 2;

	SDL_QueryTexture(texture, NULL, NULL, &width, &height);
}

void SnakeHead::update()
{
	// placeholder movement
	if (app.keyboard[SDL_SCANCODE_W])
	{
		yHeadPos -= speed;
	}
	if (app.keyboard[SDL_SCANCODE_S])
	{
		yHeadPos += speed;
	}
	if (app.keyboard[SDL_SCANCODE_A])
	{
		xHeadPos -= speed;
	}
	if (app.keyboard[SDL_SCANCODE_D])
	{
		xHeadPos += speed;
	}

	// loops the snake back into the screen
	if (xHeadPos > SCREEN_WIDTH)
	{
		xHeadPos = 0;
	}
	if (xHeadPos < 0)
	{
		xHeadPos = SCREEN_WIDTH;
	}
	if (yHeadPos > SCREEN_HEIGHT)
	{
		yHeadPos = 0;
	}
	if (yHeadPos < 0)
	{
		yHeadPos = SCREEN_HEIGHT;
	}

}

void SnakeHead::draw()
{
	blit(texture, xHeadPos, yHeadPos);
}

int SnakeHead::getXHeadPos()
{
	return xHeadPos;
}

int SnakeHead::getYHeadPos()
{
	return yHeadPos;
}

int SnakeHead::getWidth()
{
	return width;
}

int SnakeHead::getHeight()
{
	return height;
}
