#pragma once
#include "GameObject.h"
#include "common.h"
#include "draw.h"
#include <vector>

class SnakeHead :
    public GameObject
{
public: 
	void start();
	void update();
	void draw();

	int getXHeadPos();
	int getYHeadPos();
	int getWidth();
	int getHeight();
private:
	SDL_Texture* texture;
	int length; 
	int speed;

	int xHeadPos;
	int yHeadPos;

	int width;
	int height;

	// vector for the body positions
};

