#include "GameScene.h"

GameScene::GameScene()
{
	// Register and add game objects on constructor
	player = new SnakeHead();
	this->addGameObject(player);

	// 7 seconds timer
	currSpawnTimer = 420;
	spawnTime = 420;
}

GameScene::~GameScene()
{
	delete player;
}

void GameScene::start()
{
	Scene::start();
	// Initialize any scene logic here

	// Initialize spawn timer for Food
	spawnFood();
}

void GameScene::draw()
{
	Scene::draw();

	// Game UI
}

void GameScene::update()
{
	Scene::update();
	// spawn food
	// check collisions
	// if head to body 
		// run through a for loop that checks each body position 
		// if head pos = body pos -> isAlive = false 
	// if head to food -> add body
		// new body = last position of tail

	if (currSpawnTimer > 0)
		currSpawnTimer--;

	if (currSpawnTimer <= 0)
	{
		spawnFood();

		currSpawnTimer = spawnTime;
	}

	checkForCollision();
}

void GameScene::spawnFood()
{
	Food* food = new Food();
	this->addGameObject(food);

	food->setSpawnPoint(rand() % SCREEN_WIDTH, rand() % SCREEN_HEIGHT);
	spawnedFood.push_back(food);
}

void GameScene::checkForCollision()
{
	int index = -1;

	for (int i = 0; i < spawnedFood.size(); i++)
	{
		if (player->getXHeadPos() == spawnedFood[i]->getX() &&
			player->getYHeadPos() == spawnedFood[i]->getY())
		{
			index = i;
		}
	}

	if (index != -1)
	{
		std::cout << "Ate Food" << std::endl;
	}	
}
