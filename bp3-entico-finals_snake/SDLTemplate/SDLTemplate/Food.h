#pragma once
#include "GameObject.h"
#include "draw.h"
#include "common.h"

class Food :
    public GameObject
{
public:
	void start();
	void update();
	void draw();

	int getX();
	int getY();
	int getWidth();
	int getHeight();

	void setSpawnPoint(int xPos, int yPos);
private:
	SDL_Texture* texture;

	int x;
	int y;

	int width;
	int height;
};

