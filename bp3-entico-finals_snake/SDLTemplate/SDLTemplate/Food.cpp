#include "Food.h"

void Food::start()
{
	texture = loadTexture("gfx/foodSmall.png");
	width = 0;
	height = 0;

	SDL_QueryTexture(texture, NULL, NULL, &width, &height);
}

void Food::update()
{
}

void Food::draw()
{
	blit(texture, x, y);
}

int Food::getX()
{
	return x;
}

int Food::getY()
{
	return y;
}

int Food::getWidth()
{
	return width;
}

int Food::getHeight()
{
	return height;
}

void Food::setSpawnPoint(int xPos, int yPos)
{
	this->x = xPos;
	this->y = yPos;
}
