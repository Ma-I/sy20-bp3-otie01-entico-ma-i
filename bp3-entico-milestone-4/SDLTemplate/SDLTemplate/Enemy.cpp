#include "Enemy.h"
#include "Scene.h"

Enemy::Enemy()
{
}

Enemy::~Enemy()
{
}

void Enemy::start()
{
	texture = loadTexture("gfx/enemy.png");

	dirX = -1;
	dirY = 1;

	height = 0;
	width = 0;

	speed = 3;
	reloadTime = 60; // every second
	currReloadTime = 0;

	// randomize movement
	dirChangeTime = rand() % 180; // 3 seconds <- tweak this
	currDirTime = 0;

	SDL_QueryTexture(texture, NULL, NULL, &width, &height);

	sound = SoundManager::loadSound("sound/334227__jradcoolness__laser.ogg");
	sound->volume = 64;
}

void Enemy::update()
{
	x += dirX * speed;
	y += dirY * speed;

	if (currDirTime > 0)
		currDirTime--;

	if (currDirTime == 0)
	{
		dirY = -dirY;
		currDirTime = dirChangeTime;
	}

	// for bullets
	if (currReloadTime > 0)
		currReloadTime--;

	if (currReloadTime == 0)
	{
		float dx = -1;
		float dy = 0;

		// It's not working for some reason rip
		calcSlope(target->getPosX(), target->getPosY(), x, y, &dx, &dy);

		SoundManager::playSound(sound);
		Bullet* bullet = new Bullet(x + width, y - 2 + height / 2, dx, dy, 10, Side::ENEMY_SIDE);
		bullets.push_back(bullet);
		getScene()->addGameObject(bullet);
		
		currReloadTime = reloadTime;
	}

	for (int i = 0; i < bullets.size(); i++)
	{
		if (bullets[i]->getposX() < 0)
		{
			// cache variable to delete later
			Bullet* bulletToErase = bullets[i];
			bullets.erase(bullets.begin() + i);
			delete bulletToErase;

			// can't mutate vector while looping in it - will create crash if too many bullets
			// so, we must delete only one per frame
			// note: can avoid lag this way
			break;
		}
	}
}

void Enemy::draw()
{
	blit(texture, x, y);
}

void Enemy::setTarget(Player* player)
{
	target = player;
}

void Enemy::setPos(int xPos, int yPos)
{
	this->x = xPos;
	this->y = yPos;
}

int Enemy::getPosX()
{
	return x;
}

int Enemy::getPosY()
{
	return y;
}

int Enemy::getWidth()
{
	return width;
}

int Enemy::getHeight()
{
	return height;
}
