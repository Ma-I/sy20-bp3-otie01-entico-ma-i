#include "GameScene.h"

GameScene::GameScene()
{
	// Register and add game objects on constructor
	player = new Player();
	this->addGameObject(player);

	points = 0;
}

GameScene::~GameScene()
{
	delete player;
}

void GameScene::start()
{
	Scene::start();
	// Initialize any scene logic here

	initFonts();
	currentSpawnTimer = 300;
	spawnTime = 300; // every 5 seconds

	for (int i = 0; i < 3; i++)
	{
		spawn();
	}
}

void GameScene::draw()
{
	Scene::draw();

	drawText(110, 20, 255, 255, 255, TEXT_CENTER, "POINTS: %03d", points);

	if (player->getIsAlive() == false)
	{
		drawText(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, 255, 255, 255, TEXT_CENTER, "GAME OVER!");
	}
}

void GameScene::update()
{
	Scene::update();

	doSpawnLogic();
	doCollisionLogic();
}

void GameScene::doSpawnLogic()
{
	if (currentSpawnTimer > 0)
		currentSpawnTimer--;

	if (currentSpawnTimer <= 0)
	{
		for (int i = 0; i < 3; i++)
		{
			spawn();
		}

		currentSpawnTimer = spawnTime;
	}
}

void GameScene::doCollisionLogic()
{
	// Checks for collisions
	for (int i = 0; i < objects.size(); i++)
	{
		// Cast to bullet (dynamic cast)
		Bullet* bullet = dynamic_cast<Bullet*>(objects[i]);
		// if (objects[i] is bullet) <- for C#

		// Check if case was successful (i.e. objects[i] is a Bullet)
		if (bullet != NULL)
		{
			// Checks where the bullet is coming from - player or enemy
			// Checks for enemy's bullet
			if (bullet->getSide() == Side::ENEMY_SIDE)
			{
				// Parameters can be typed in one line or like this:
				int collision = checkCollision(
					player->getPosX(), player->getPosY(), player->getWidth(), player->getHeight(),
					bullet->getposX(), bullet->getposY(), bullet->getWidth(), bullet->getHeight()
				);

				if (collision == 1)
				{
					player->doDeath();
					break;
				}
			}
			// Checks against ALL enemies:
			else if (bullet->getSide() == Side::PLAYER_SIDE)
			{
				for (int i = 0; i < spawnedEnemies.size(); i++)
				{
					Enemy* currentEnemy = spawnedEnemies[i];

					int collision = checkCollision(
						currentEnemy->getPosX(), currentEnemy->getPosY(), currentEnemy->getWidth(), currentEnemy->getHeight(),
						bullet->getposX(), bullet->getposY(), bullet->getWidth(), bullet->getHeight()
					);

					if (collision == 1)
					{
						despawnEnemy(currentEnemy);
						// IMPORTANT NOTE: Despawn only ONE AT A TIME
						// Or a crash might happen bc of looping while deleting

						// Increment points
						points++;
						break;
					}
				}

			}
		}
	}
}

void GameScene::spawn()
{
	Enemy* enemy = new Enemy();
	this->addGameObject(enemy);
	enemy->setTarget(player);

	enemy->setPos(1200, rand() % 200 + 400);
	spawnedEnemies.push_back(enemy);
}

void GameScene::despawnEnemy(Enemy* enemy)
{
	int index = -1;
	for (int i = 0; i < spawnedEnemies.size(); i++)
	{
		// Break once the index has been found
		if (enemy == spawnedEnemies[i])
		{
			index = i;
			break;
		}
	}

	// once object is found
	// Collision works but deleting does not happen
	if (index != -1)
	{
		spawnedEnemies.erase(spawnedEnemies.begin() + index);
		delete enemy;
	}
}
