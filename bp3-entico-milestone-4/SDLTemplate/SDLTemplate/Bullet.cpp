#include "Bullet.h"

Bullet::Bullet(float posX, float posY, float dirX, float dirY, float speed, Side side)
{
	this->x = posX;
	this->y = posY;
	this->directionX = dirX;
	this->directionY = dirY;
	this->speed = speed;
	this->side = side;
}

void Bullet::start()
{
	if (side == Side::PLAYER_SIDE)
	{
		texture = loadTexture("gfx/playerBullet.png");
	}
	else
	{
		texture = loadTexture("gfx/alienBullet.png");
	}

	height = 0;
	width = 0;

	SDL_QueryTexture(texture, NULL, NULL, &width, &height);
}

void Bullet::update()
{
	x += directionX * speed;
	y += directionY * speed;
}

void Bullet::draw()
{
	blit(texture, x, y);
}

float Bullet::getposX()
{
	return x;
}

float Bullet::getposY()
{
	return y;
}

float Bullet::getWidth()
{
	return width;
}

float Bullet::getHeight()
{
	return height;
}

Side Bullet::getSide()
{
	return side;
}
