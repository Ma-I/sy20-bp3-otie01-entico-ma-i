#include "Player.h"

void Player::start()
{
	// Load texture
	texture = loadTexture("gfx/player.png");

	// Initialize variables (To avoid garbage values)
	x = 0; 
	y = 0;
	width = 0; 
	height = 0;
	speed = 5;

	// Find out width and height / Query texture
	SDL_QueryTexture(texture, NULL, NULL, &width, &height);
}

void Player::update()
{
	if (app.keyboard[SDL_SCANCODE_W])
	{
		y -= speed;
	}
	if (app.keyboard[SDL_SCANCODE_A])
	{
		x -= speed;
	}
	if (app.keyboard[SDL_SCANCODE_S])
	{
		y += speed;
	}
	if (app.keyboard[SDL_SCANCODE_D])
	{
		x += speed;
	}
}

void Player::draw()
{
	blit(texture, x, y);
}
