#pragma once
#include "GameObject.h"
#include "common.h"
#include "draw.h"

class Bullet :
    public GameObject
{
public:
	Bullet(float posX, float posY, float dirX, float dirY, float speed);
	void start();
	void update();
	void draw();
	float getposX();
	float getposY();
	float getWidth();
	float getHeight();
private:
	SDL_Texture* texture;
	int x;
	int y;
	int width;
	int height;
	int speed;
	int directionX;
	int directionY;
};

